import { Hero } from './hero';

export const HEROES: Hero[] = [
  { id: 11, name: 'Kaliman' },
  { id: 12, name: 'Dragon Ball' },
  { id: 13, name: 'El Santo' },
  { id: 14, name: 'Blue Daemon' },
  { id: 15, name: 'El Chapulin Colorado' },
  { id: 16, name: 'Memin Pinguin' },
  { id: 17, name: 'Condorito' },
  { id: 18, name: 'Pikachu' },
  { id: 19, name: 'Los hermanos Aldama' },
  { id: 20, name: 'Mama Lucha' }
];